package com.aardwark.kw.user_management.controller;

import com.aardwark.kw.user_management.controller.dto.UserDto;
import com.aardwark.kw.user_management.controller.dto.UserUpdateDto;
import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.entity.UserState;
import com.aardwark.kw.user_management.mapper.UserMapper;
import com.aardwark.kw.user_management.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.text.MessageFormat.format;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/users", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
@Slf4j
public class UserController {

    private final UserService userService;
    private final UserMapper mapper;

    public UserController(UserService userService, UserMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping()
    public ResponseEntity<Object> getAllUsers() {
        log.info("Get all users");
        List<UserDto> allUsers = userService.getAllUsers()
                                            .stream()
                                            .map(mapper::mapToDto)
                                            .collect(toList());
        return new ResponseEntity<>(allUsers, OK);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getUser(@PathVariable final UUID id) {
        log.info("Get user id: {}", id);
        Optional<User> user = userService.getUser(id);
        return user.map(value -> new ResponseEntity<Object>(mapper.mapToDto(value), OK))
                   .orElseGet(() -> new ResponseEntity<>(format("User with id: {0} does not exists", id), NOT_FOUND));
    }

    @PostMapping
    public ResponseEntity<Object> createUser(@Valid @RequestBody final UserDto user) {
        log.info("Create new user: {}", user);
        var createdUser = userService.createUser(mapper.mapToEntity(user));
        return new ResponseEntity<>(mapper.mapToDto(createdUser), CREATED);
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable final UUID id, @Valid @RequestBody final UserUpdateDto user) {
        log.info("Update user: {}", user);
        var updatedUser = userService.updateUser(id, mapper.mapToEntity(user));
        return new ResponseEntity<>(mapper.mapToDto(updatedUser), OK);
    }

    @PatchMapping(value = "/{id}")
    public ResponseEntity<Object> changeUserState(@PathVariable final UUID id, @RequestParam final String newState) {
        log.info("Change user id: {} status to: {}", id, newState);
        var userState = getUserState(newState);
        if (nonNull(userState)) {
            userService.updateUserState(id, userState);
            return new ResponseEntity<>(OK);
        } else {
            return new ResponseEntity<>(format("User state with name: {0} does not exists", newState), BAD_REQUEST);
        }
    }

    private UserState getUserState(String newState) {
        try {
            return UserState.valueOf(newState.toUpperCase());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
