package com.aardwark.kw.user_management.service.impl;

import com.aardwark.kw.domain_events.user.UserCreated;
import com.aardwark.kw.domain_events.user.UserEvent;
import com.aardwark.kw.domain_events.user.UserStateChanged;
import com.aardwark.kw.domain_events.user.UserUpdated;
import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.mapper.KafkaMapper;
import com.aardwark.kw.user_management.repository.UserRepository;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.aardwark.kw.user_management.entity.UserState.ACTIVE;
import static java.util.Collections.emptyList;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class UserServicePublishingTest {

    private static final UUID ID = UUID.randomUUID();
    private static final String TOPIC = "topic";
    private static final User USER = new User();

    private final UserRepository userRepository = mock(UserRepository.class);
    private final KafkaMapper kafkaMapper = mock(KafkaMapper.class);
    private final KafkaTemplate<UUID, UserEvent> kafkaTemplate = mock(KafkaTemplate.class);

    private final UserServicePublishing userServicePublishing = new UserServicePublishing(userRepository, kafkaTemplate,
                                                                                          kafkaMapper, TOPIC);


    @BeforeEach
    void setUp() {
        USER.setId(ID);
    }

    @Test
    void doesReturnEmptyListOfUsers() {
        //GIVEN
        when(userRepository.findAll()).thenReturn(emptyList());
        //WHEN
        List<User> users = userServicePublishing.getAllUsers();
        //THEN
        assertThat(users).isEqualTo(emptyList());
        verify(userRepository).findAll();
    }

    @Test
    void doesReturnListOfUsers() {
        //GIVEN
        when(userRepository.findAll()).thenReturn(List.of(USER));
        //WHEN
        List<User> users = userServicePublishing.getAllUsers();
        //THEN
        assertThat(users).isEqualTo(List.of(USER));
        verify(userRepository).findAll();
    }

    @Test
    void doesGetUserById() {
        //GIVEN
        when(userRepository.findById(any())).thenReturn(of(USER));
        //WHEN
        Optional<User> user = userServicePublishing.getUser(ID);
        //THEN
        assertThat(user).isPresent()
                        .contains(USER);
        verify(userRepository).findById(ID);
    }

    @Test
    void doesCreateUser() {
        //GIVEN
        UserCreated userCreated = UserCreated.builder()
                                             .id(ID)
                                             .build();
        when(userRepository.save(any())).thenReturn(USER);
        when(kafkaMapper.mapToCreated(any())).thenReturn(userCreated);
        //WHEN
        User user = userServicePublishing.createUser(USER);
        //THEN
        assertThat(user).isEqualTo(USER);
        verify(userRepository).save(USER);
        verify(kafkaMapper).mapToCreated(USER);
        verify(kafkaTemplate).send(any(ProducerRecord.class));
    }

    @Test
    void doesUpdateUser() {
        //GIVEN
        UserUpdated userUpdated = UserUpdated.builder()
                                             .id(ID)
                                             .build();
        when(userRepository.findById(any())).thenReturn(of(USER));
        when(userRepository.save(any())).thenReturn(USER);
        when(kafkaMapper.mapToUpdated(any())).thenReturn(userUpdated);
        //WHEN
        User user = userServicePublishing.updateUser(ID, USER);
        //THEN
        assertThat(user).isEqualTo(USER);
        verify(userRepository).findById(ID);
        verify(userRepository).save(USER);
        verify(kafkaMapper).mapToUpdated(USER);
        verify(kafkaTemplate).send(any(ProducerRecord.class));
    }

    @Test
    void doesUpdateUserState() {
        //GIVEN
        UserStateChanged userStateChange = UserStateChanged.builder()
                                                           .id(ID)
                                                           .build();
        when(userRepository.findById(any())).thenReturn(of(USER));
        when(kafkaMapper.mapToStateChanged(any(), any(), any())).thenReturn(userStateChange);
        //WHEN
        userServicePublishing.updateUserState(ID, ACTIVE);
        //THEN
        verify(userRepository, times(2)).findById(ID);
        verify(userRepository).save(USER);
        verify(kafkaMapper).mapToStateChanged(ID, null, ACTIVE);
        verify(kafkaTemplate).send(any(ProducerRecord.class));
    }

    @Test
    void doesDeleteUser() {
        //GIVEN
        when(userRepository.existsById(any())).thenReturn(true);
        //WHEN
        userServicePublishing.deleteUser(ID);
        //THEN
        verify(userRepository).existsById(ID);
        verify(userRepository).deleteById(ID);
    }

}