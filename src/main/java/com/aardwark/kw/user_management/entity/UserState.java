package com.aardwark.kw.user_management.entity;

import lombok.Getter;

@Getter
public enum UserState {
    ACTIVE,
    INACTIVE,
    BLOCKED,
    DELETED
}
