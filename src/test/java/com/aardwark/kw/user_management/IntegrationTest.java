package com.aardwark.kw.user_management;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(TYPE)
@Retention(RUNTIME)

@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(initializers = {IntegrationTestConfig.Initializer.class})
@TestPropertySource(properties = "spring.config.location=classpath:application-test.properties")
public @interface IntegrationTest {
}
