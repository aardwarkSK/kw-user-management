package com.aardwark.kw.user_management.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class UserManagementProperties {

    @Value("${spring.kafka.bootstrap}")
    private String bootstrapServer;

    @Value("${spring.kafka.producer.clientId}")
    private String producerId;

    @Value("${spring.kafka.user.management.topic}")
    private String usersTopic;

    @Value("${spring.kafka.trusted.packages}")
    private String trustedPackages;

}
