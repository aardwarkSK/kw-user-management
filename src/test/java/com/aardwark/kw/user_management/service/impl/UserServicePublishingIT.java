package com.aardwark.kw.user_management.service.impl;

import com.aardwark.kw.domain_events.user.UserCreated;
import com.aardwark.kw.domain_events.user.UserEvent;
import com.aardwark.kw.domain_events.user.UserStateChanged;
import com.aardwark.kw.domain_events.user.UserUpdated;
import com.aardwark.kw.user_management.EmbeddedKafkaTest;
import com.aardwark.kw.user_management.config.UserManagementProperties;
import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.entity.UserState;
import com.aardwark.kw.user_management.repository.UserRepository;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static com.aardwark.kw.user_management.entity.UserState.ACTIVE;
import static com.aardwark.kw.user_management.entity.UserState.DELETED;
import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;
import static java.time.LocalDate.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

class UserServicePublishingIT extends EmbeddedKafkaTest {

    @Autowired
    private Flyway flyway;

    @Autowired
    private UserManagementProperties userManagementProperties;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserServicePublishing userServicePublishing;

    private KafkaConsumer<UUID, UserEvent> usersConsumer;

    @BeforeAll
    void setUp() {
        usersConsumer = createUsersConsumer(userManagementProperties.getBootstrapServer(),
                                            Set.of(userManagementProperties.getUsersTopic()),
                                            userManagementProperties.getTrustedPackages(),
                                            "it_test_consumer");
    }

    @AfterEach
    void tearDown() {
        flyway.clean();
        flyway.migrate();
    }

    @Test
    void shouldReturnEmptyUserList() {
        //WHEN
        List<User> allUsers = userServicePublishing.getAllUsers();
        //THEN
        assertThat(allUsers).isEmpty();
    }

    @Test
    void shouldGetAllUsers() {
        //GIVEN
        var user1 = createUserEntity();
        var user2 = createUserEntity();
        //WHEN
        List<User> allUsers = userServicePublishing.getAllUsers();
        //THEN
        assertThat(allUsers).hasSize(2);
        assertThat(allUsers.get(0)).isEqualTo(user1);
        assertThat(allUsers.get(1)).isEqualTo(user2);
    }

    @Test
    void shouldGetUserNonExistingUser() {
        //WHEN
        Optional<User> user = userServicePublishing.getUser(UUID.randomUUID());
        //THEN
        assertThat(user).isEmpty();
    }

    @Test
    void shouldGetUser() {
        //GIVEN
        var dbUser = createUserEntity();
        //WHEN
        Optional<User> user = userServicePublishing.getUser(dbUser.getId());
        //THEN
        assertThat(user).isPresent()
                        .get()
                        .isEqualTo(dbUser);
    }

    @Test
    void shouldCreateUser() {
        //GIVEN
        var userForCreation = createUserEntity();
        var createdEvent = getCreatedEvent(userForCreation);
        //WHEN
        userServicePublishing.createUser(userForCreation);
        //THEN
        assertThat(userServicePublishing.getUser(userForCreation.getId())).isPresent()
                                                                          .get()
                                                                          .isEqualTo(userForCreation);
        await().atMost(ofSeconds(3))
               .pollInterval(ofMillis(500))
               .until(() -> isConsumedMessage(usersConsumer, createdEvent));
        usersConsumer.commitSync();
    }

    @Test
    void shouldUpdateUser() {
        //GIVEN
        var user = createUserEntity();
        user.setAddress("new_address");
        user.setFirstName("new_one");
        var updatedEvent = getUpdatedEvent(user);
        //WHEN
        userServicePublishing.updateUser(user.getId(), user);
        //THEN
        assertThat(userServicePublishing.getUser(user.getId())).isPresent()
                                                               .get()
                                                               .isEqualTo(user);
        await().atMost(ofSeconds(3))
               .pollInterval(ofMillis(500))
               .until(() -> isConsumedMessage(usersConsumer, updatedEvent));
        usersConsumer.commitSync();
    }

    @Test
    void shouldChangeUserState() {
        //GIVEN
        UserState newState = DELETED;
        var user = createUserEntity();
        var userStateChange = UserStateChanged.builder()
                                              .id(user.getId())
                                              .oldState(user.getState()
                                                            .name())
                                              .newState(newState.name())
                                              .build();
        //WHEN
        userServicePublishing.updateUserState(user.getId(), newState);
        //THEN
        assertThat(userServicePublishing.getUser(user.getId())).isPresent()
                                                               .get()
                                                               .hasFieldOrPropertyWithValue("state", DELETED);
        await().atMost(ofSeconds(3))
               .pollInterval(ofMillis(500))
               .until(() -> isConsumedMessage(usersConsumer, userStateChange));
        usersConsumer.commitSync();
    }

    @Test
    void shouldDeleteUser() {
        //GIVEN
        var user = createUserEntity();
        //WHEN
        assertThat(userRepository.findById(user.getId())).isPresent();
        userServicePublishing.deleteUser(user.getId());
        //THEN
        assertThat(userRepository.findById(user.getId())).isEmpty();
    }

    private User createUserEntity() {
        User user = User.builder()
                        .id(UUID.randomUUID())
                        .firstName("Kafka")
                        .lastName("Workshop")
                        .dayOfBirth(now())
                        .address("Aardwark")
                        .state(ACTIVE)
                        .build();
        return userRepository.save(user);
    }

    private UserCreated getCreatedEvent(User user) {
        return UserCreated.builder()
                          .id(user.getId())
                          .address(user.getAddress())
                          .dayOfBirth(user.getDayOfBirth())
                          .firstName(user.getFirstName())
                          .lastName(user.getLastName())
                          .state(user.getState()
                                     .name())
                          .build();
    }

    private UserUpdated getUpdatedEvent(User user) {
        return UserUpdated.builder()
                          .id(user.getId())
                          .firstName(user.getFirstName())
                          .lastName(user.getLastName())
                          .address(user.getAddress())
                          .build();
    }
}