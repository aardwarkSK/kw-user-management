package com.aardwark.kw.user_management.service.impl;

import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.entity.UserState;
import com.aardwark.kw.user_management.repository.UserRepository;
import com.aardwark.kw.user_management.service.UserNotFoundException;
import com.aardwark.kw.user_management.service.UserService;
import com.aardwark.kw.user_management.service.UserServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.RecoverableDataAccessException;
import org.springframework.dao.TransientDataAccessException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.text.MessageFormat.format;

@Slf4j
public class UserServiceDefault implements UserService {

    private final UserRepository userRepository;

    public UserServiceDefault(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> allUsers = userRepository.findAll();
        log.info("Returned {} users", allUsers.size());
        log.trace("All users are: {}", allUsers);
        return allUsers;
    }

    @Override
    public Optional<User> getUser(final UUID id) {
        Optional<User> user = userRepository.findById(id);
        log.debug("User data from DB: {}", user);
        return user;
    }

    @Override
    public User createUser(final User user) {
        try {
            user.setId(UUID.randomUUID());
            log.debug("Creating new user: {}", user);
            return userRepository.save(user);
        } catch (RecoverableDataAccessException | TransientDataAccessException e) {
            throw new UserServiceException(format("Unable to create user due: {0}", e.getMessage()), e);
        }
    }

    @Override
    public User updateUser(final UUID id, final User user) {
        log.debug("Updating user: {}", id);
        var userFromDb = getUser(id).orElseThrow(
                () -> new UserNotFoundException(format("User with id: {0} does not exists", id)));
        user.setId(userFromDb.getId());
        user.setState(userFromDb.getState());
        user.setDayOfBirth(userFromDb.getDayOfBirth());
       return userRepository.save(user);
    }

    @Override
    public void updateUserState(final UUID id, final UserState newState) {
        log.debug("Updating user: {} state", id);
        var user = getUser(id).orElseThrow(
                () -> new UserNotFoundException(format("User with id: {0} does not exists", id)));
        user.setState(newState);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(final UUID id) {
        boolean exists = userRepository.existsById(id);
        log.info("User with id {} existing on db: {}", id, exists);
        if (exists) {
            userRepository.deleteById(id);
        } else {
            throw new UserNotFoundException(format("User with id: {0} does not exists", id));
        }
    }

}
