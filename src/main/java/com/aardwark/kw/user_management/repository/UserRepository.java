package com.aardwark.kw.user_management.repository;

import com.aardwark.kw.user_management.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Spring data repository interface used for access and modify users data in database.
 */
public interface UserRepository extends JpaRepository<User, UUID> {
}
