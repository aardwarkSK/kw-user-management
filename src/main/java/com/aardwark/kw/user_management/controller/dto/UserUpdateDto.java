package com.aardwark.kw.user_management.controller.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Data
@NoArgsConstructor
@SuperBuilder
public class UserUpdateDto {
    private UUID id;
    private String firstName;

    @NotBlank(message = "Last name is mandatory")
    private  String lastName;

    private String address;
}
