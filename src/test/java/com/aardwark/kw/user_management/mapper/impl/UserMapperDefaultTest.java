package com.aardwark.kw.user_management.mapper.impl;

import com.aardwark.kw.user_management.controller.dto.UserDto;
import com.aardwark.kw.user_management.controller.dto.UserUpdateDto;
import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.entity.UserState;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.UUID;

import static com.aardwark.kw.user_management.entity.UserState.ACTIVE;
import static org.assertj.core.api.Assertions.assertThat;

class UserMapperDefaultTest {

    public static final UUID ID = UUID.randomUUID();
    public static final String FIRST_NAME = "Jozko";
    public static final String LAST_NAME = "Dezko";
    public static final String ADDRESS = "ABCD";
    public static final LocalDate DAY_OF_BIRTH = LocalDate.of(2000, 1, 1);
    public static final UserState USER_STATE = ACTIVE;
    private final UserMapperDefault mapper = new UserMapperDefault();


    @Test
    void doesMapToDto() {
        //GIVEN
        User userEntity = createUserEntity();
        //WHEN
        UserDto mappedDto = mapper.mapToDto(userEntity);
        //THEN
        assertThat(mappedDto).isNotNull();
        assertThat(mappedDto.getId()).isEqualTo(ID);
        assertThat(mappedDto.getFirstName()).isEqualTo(FIRST_NAME);
        assertThat(mappedDto.getLastName()).isEqualTo(LAST_NAME);
        assertThat(mappedDto.getAddress()).isEqualTo(ADDRESS);
        assertThat(mappedDto.getDayOfBirth()).isEqualTo(DAY_OF_BIRTH);
        assertThat(mappedDto.getState()).isEqualTo(USER_STATE.name());
    }

    @Test
    void doesMapToEntityForEmptyId() {
        //GIVEN
        UserDto userResponse = createUserResponse();
        userResponse.setId(null);
        //WHEN
        User mappedDto = mapper.mapToEntity(userResponse);
        //THEN
        assertThat(mappedDto).isNotNull();
        assertThat(mappedDto.getId()).isNull();
        assertThat(mappedDto.getFirstName()).isEqualTo(FIRST_NAME);
        assertThat(mappedDto.getLastName()).isEqualTo(LAST_NAME);
        assertThat(mappedDto.getAddress()).isEqualTo(ADDRESS);
        assertThat(mappedDto.getDayOfBirth()).isEqualTo(DAY_OF_BIRTH);
        assertThat(mappedDto.getState()).isEqualTo(USER_STATE);
    }

    @Test
    void doesMapToEntityForUpdateDto() {
        //GIVEN
        UserUpdateDto userUpdate = createUserUpdate();
        //WHEN
        User mappedDto = mapper.mapToEntity(userUpdate);
        //THEN
        assertThat(mappedDto).isNotNull();
        assertThat(mappedDto.getId()).isEqualTo(ID);
        assertThat(mappedDto.getFirstName()).isEqualTo(FIRST_NAME);
        assertThat(mappedDto.getLastName()).isEqualTo(LAST_NAME);
        assertThat(mappedDto.getAddress()).isEqualTo(ADDRESS);
        assertThat(mappedDto.getDayOfBirth()).isNull();
        assertThat(mappedDto.getState()).isNull();
    }

    @Test
    void doesMapEntity() {
        //GIVEN
        UserDto userResponse = createUserResponse();
        //WHEN
        User mappedDto = mapper.mapToEntity(userResponse);
        //THEN
        assertThat(mappedDto).isNotNull();
        assertThat(mappedDto.getId()).isNotEqualTo(ID);
        assertThat(mappedDto.getFirstName()).isEqualTo(FIRST_NAME);
        assertThat(mappedDto.getLastName()).isEqualTo(LAST_NAME);
        assertThat(mappedDto.getAddress()).isEqualTo(ADDRESS);
        assertThat(mappedDto.getDayOfBirth()).isEqualTo(DAY_OF_BIRTH);
        assertThat(mappedDto.getState()).isEqualTo(USER_STATE);
    }

    private User createUserEntity() {
        return User.builder()
                   .id(ID)
                   .firstName(FIRST_NAME)
                   .lastName(LAST_NAME)
                   .dayOfBirth(DAY_OF_BIRTH)
                   .address(ADDRESS)
                   .state(USER_STATE)
                   .build();
    }

    private UserDto createUserResponse() {
        return UserDto.builder()
                      .id(ID)
                      .firstName(FIRST_NAME)
                      .lastName(LAST_NAME)
                      .dayOfBirth(DAY_OF_BIRTH)
                      .address(ADDRESS)
                      .state(USER_STATE.name())
                      .build();
    }

    private UserUpdateDto createUserUpdate() {
        return UserUpdateDto.builder()
                      .id(ID)
                      .firstName(FIRST_NAME)
                      .lastName(LAST_NAME)
                      .address(ADDRESS)
                      .build();
    }
}