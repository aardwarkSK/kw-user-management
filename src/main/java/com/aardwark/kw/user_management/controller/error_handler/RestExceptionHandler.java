package com.aardwark.kw.user_management.controller.error_handler;

import com.aardwark.kw.user_management.service.UserNotFoundException;
import com.aardwark.kw.user_management.service.UserServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.text.MessageFormat;

import static java.util.stream.Collectors.joining;
import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Order(HIGHEST_PRECEDENCE)
@ControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error(ex.getMessage());
        String errorBody = "Malformed JSON request" + ex.getLocalizedMessage();
        return new ResponseEntity<>(errorBody, BAD_REQUEST);
    }


    @ExceptionHandler(UserServiceException.class)
    public ResponseEntity<Object> handleUserLogicException(final UserServiceException userServiceException) {
        log.error(userServiceException.getMessage());
        return new ResponseEntity<>(userServiceException.getMessage(), BAD_REQUEST);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Object> handleNotFoundException(final UserNotFoundException userNotFoundException) {
        log.error(userNotFoundException.getMessage());
        return new ResponseEntity<>(userNotFoundException.getMessage(), NOT_FOUND);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException argumentNotValidException,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        log.error(argumentNotValidException.getMessage());
        return new ResponseEntity<>(getAllArgumentsErrorsAsString(argumentNotValidException), BAD_REQUEST);
    }

    private String getAllArgumentsErrorsAsString(MethodArgumentNotValidException argumentNotValidException) {
        String objectName = argumentNotValidException.getBindingResult()
                                                     .getObjectName();
        String errors = argumentNotValidException.getBindingResult()
                                                  .getFieldErrors()
                                                  .stream()
                                                  .map(DefaultMessageSourceResolvable::getDefaultMessage)
                                                  .collect(joining(", "));
        return "Validation errors occurs on object " + objectName + ": " + errors;
    }
}
