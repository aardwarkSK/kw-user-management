package com.aardwark.kw.user_management.mapper;

import com.aardwark.kw.domain_events.user.UserCreated;
import com.aardwark.kw.domain_events.user.UserStateChanged;
import com.aardwark.kw.domain_events.user.UserUpdated;
import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.entity.UserState;

import java.util.UUID;

public interface KafkaMapper {

    /**
     * Map db entity to UserCreated Kafka model
     * @param user DB entity
     * @return UserCreated Kafka model
     */
    UserCreated mapToCreated(User user);

    /**
     * Map db entity to UserUpdated Kafka model
     * @param user DB entity
     * @return UserUpdated Kafka model
     */
    UserUpdated mapToUpdated(User user);

    /**
     * Map db entity to UserStateChange Kafka model
     * @param id user identifier
     * @param oldState previous user state
     * @param newState new user state
     * @return UserStateChange Kafka model
     */
    UserStateChanged mapToStateChanged(UUID id, UserState oldState, UserState newState);
}
