package com.aardwark.kw.user_management.service.impl;

import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.repository.UserRepository;
import com.aardwark.kw.user_management.service.UserNotFoundException;
import com.aardwark.kw.user_management.service.UserServiceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.RecoverableDataAccessException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.aardwark.kw.user_management.entity.UserState.ACTIVE;
import static java.util.Collections.emptyList;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceDefaultTest {
    private final static UUID ID = UUID.randomUUID();

    @Mock
    private UserRepository userRepository;
    @InjectMocks
    private UserServiceDefault userService;

    private static final User USER = new User();

    @BeforeEach
    void setUp() {
        USER.setId(ID);
    }

    @Test
    void doesReturnEmptyListOfUsers() {
        //GIVEN
        when(userRepository.findAll()).thenReturn(emptyList());
        //WHEN
        List<User> users = userService.getAllUsers();
        //THEN
        assertThat(users).isEqualTo(emptyList());
        verify(userRepository).findAll();
    }

    @Test
    void doesReturnListOfUsers() {
        //GIVEN
        when(userRepository.findAll()).thenReturn(List.of(USER));
        //WHEN
        List<User> users = userService.getAllUsers();
        //THEN
        assertThat(users).isEqualTo(List.of(USER));
        verify(userRepository).findAll();
    }

    @Test
    void doesReturnEmptyForNotExistingUser() {
        //GIVEN
        when(userRepository.findById(any())).thenReturn(empty());
        //WHEN
        Optional<User> user = userService.getUser(ID);
        //THEN
        assertThat(user).isEmpty();
        verify(userRepository).findById(ID);
    }

    @Test
    void doesGetUserById() {
        //GIVEN
        when(userRepository.findById(any())).thenReturn(of(USER));
        //WHEN
        Optional<User> user = userService.getUser(ID);
        //THEN
        assertThat(user).isPresent()
                        .contains(USER);
        verify(userRepository).findById(ID);
    }

    @Test
    void handleJpaExceptionForAddUser() {
        //GIVEN
        when(userRepository.save(any())).thenThrow(new RecoverableDataAccessException("reason"));
        //WHEN
        assertThatThrownBy(() -> userService.createUser(USER)).isInstanceOf(UserServiceException.class)
                                                              .hasMessage("Unable to create user due: reason");
    }

    @Test
    void doesCreateUser() {
        //GIVEN
        when(userRepository.save(any())).thenReturn(USER);
        //WHEN
        User user = userService.createUser(USER);
        //THEN
        assertThat(user).isEqualTo(USER);
        verify(userRepository).save(USER);
    }

    @Test
    void throwsExceptionForNotExistingUserForUpdate() {
        //GIVEN
        when(userRepository.findById(any())).thenReturn(empty());
        //WHEN
        assertThatThrownBy(() -> userService.updateUser(ID, USER)).isInstanceOf(UserNotFoundException.class)
                                                                  .hasMessage("User with id: " + ID.toString() + " does not exists");
        //THEN
        verify(userRepository).findById(ID);
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    void doesUpdateUser() {
        //GIVEN
        when(userRepository.findById(any())).thenReturn(of(USER));
        when(userRepository.save(any())).thenReturn(USER);
        //WHEN
        userService.updateUser(ID, USER);
        //THEN
        verify(userRepository).findById(ID);
        verify(userRepository).save(USER);
    }

    @Test
    void throwsExceptionForNotExistingUserForUpdateState() {
        //GIVEN
        when(userRepository.findById(any())).thenReturn(empty());
        //WHEN
        assertThatThrownBy(() -> userService.updateUserState(ID, ACTIVE)).isInstanceOf(UserNotFoundException.class)
                                                                         .hasMessage("User with id: " + ID.toString() + " does not exists");
        //THEN
        verify(userRepository).findById(ID);
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    void doesUpdateUserState() {
        //GIVEN
        when(userRepository.findById(any())).thenReturn(of(USER));
        //WHEN
        userService.updateUserState(ID, ACTIVE);
        //THEN
        verify(userRepository).findById(ID);
        verify(userRepository).save(USER);
    }

    @Test
    void throwsExceptionForNotExistingUserForDeletion() {
        //GIVEN
        when(userRepository.existsById(any())).thenReturn(false);
        //WHEN
        assertThatThrownBy(() -> userService.deleteUser(ID)).isInstanceOf(UserNotFoundException.class)
                                                            .hasMessage("User with id: " + ID.toString() + " does not exists");
        //THEN
        verify(userRepository).existsById(ID);
        verifyNoMoreInteractions(userRepository);
    }

    @Test
    void doesDeleteUser() {
        //GIVEN
        when(userRepository.existsById(any())).thenReturn(true);
        //WHEN
        userService.deleteUser(ID);
        //THEN
        verify(userRepository).existsById(ID);
        verify(userRepository).deleteById(ID);
    }

}