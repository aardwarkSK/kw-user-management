package com.aardwark.kw.user_management;

import com.aardwark.kw.domain_events.user.UserEvent;
import com.aardwark.kw.user_management.controller.dto.UserDto;
import com.aardwark.kw.user_management.controller.dto.UserUpdateDto;
import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static com.aardwark.kw.user_management.entity.UserState.ACTIVE;
import static com.aardwark.kw.user_management.entity.UserState.DELETED;
import static java.time.LocalDate.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@IntegrationTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserManagementApplicationIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private Flyway flyway;

    @MockBean
    private KafkaTemplate<UUID, UserEvent> kafkaTemplate;

    @AfterEach
    void tearDown() {
        flyway.clean();
        flyway.migrate();
    }

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    @SneakyThrows
    void shouldReturn415ForWrongContentType() {
        mockMvc.perform(get("/users"))
               .andExpect(status().is(415));
    }

    @Test
    @SneakyThrows
    void shouldReturnEmptyListOfUsers() {
        mockMvc.perform(get("/users").header("Content-Type", "application/json"))
               .andExpect(status().isOk())
               .andExpect(jsonPath("$", empty()));
    }

    @Test
    @SneakyThrows
    void shouldNotCreateUserWithoutDayOfBirth() {
        //GIVEN
        UserDto userResponse = createUserDto(null, null, "Workshop");
        String jsonInput = objectMapper.writeValueAsString(userResponse);
        //THEN
        mockMvc.perform(post("/users").header("Content-Type", "application/json")
                                      .content(jsonInput))
               .andExpect(status().isBadRequest())
               .andExpect(jsonPath("$").value("Validation errors occurs on object userDto: Day of birth is mandatory"));
    }

    @Test
    @SneakyThrows
    void shouldNotCreateUserWithoutLastName() {
        //GIVEN
        UserDto userResponse = createUserDto(null, LocalDate.of(2020, 1, 1), null);
        String jsonInput = objectMapper.writeValueAsString(userResponse);
        //THEN
        mockMvc.perform(post("/users").header("Content-Type", "application/json")
                                      .content(jsonInput))
               .andExpect(status().isBadRequest())
               .andExpect(jsonPath("$").value("Validation errors occurs on object userDto: Last name is mandatory"));
    }

    @Test
    @SneakyThrows
    void shouldCreateUser() {
        //GIVEN
        UserDto userInput = createUserDto(null);
        String jsonInput = objectMapper.writeValueAsString(userInput);
        //THEN
        mockMvc.perform(post("/users").header("Content-Type", "application/json")
                                      .content(jsonInput))
               .andExpect(status().isCreated())
               .andExpect(jsonPath("$.firstName").value("Kafka"))
               .andExpect(jsonPath("$.lastName").value("Workshop"))
               .andExpect(jsonPath("$.dayOfBirth").value(now().toString()))
               .andExpect(jsonPath("$.address").value("Aardwark"))
               .andExpect(jsonPath("$.state").value("ACTIVE"));
        List<User> allUsers = userRepository.findAll();
        assertThat(allUsers).isNotEmpty();
        assertThat(allUsers.size()).isEqualTo(1);
        assertThat(allUsers.get(0).getFirstName()).isEqualTo("Kafka");
        verify(kafkaTemplate).send(any(ProducerRecord.class));
    }

    @Test
    @SneakyThrows
    void shouldGetUser() {
        //GIVEN
        User userEntity = createUserEntity();
        var userId = userRepository.save(userEntity).getId();
        UserDto userInput = createUserDto(userId);
        String jsonOutput = objectMapper.writeValueAsString(userInput);
        //THEN
        mockMvc.perform(get("/users/" + userId).header("Content-Type", "application/json"))
               .andExpect(status().isOk())
               .andExpect(content().json(jsonOutput));
    }

    @Test
    @SneakyThrows
    void shouldReturnNotFoundForNotExistingUser() {
        //GIVEN
        var wrongUserId = UUID.randomUUID();
        //THEN
        mockMvc.perform(get("/users/" +wrongUserId).header("Content-Type", "application/json"))
               .andExpect(status().isNotFound())
               .andExpect(jsonPath("$").value("User with id: " + wrongUserId + " does not exists"));
    }

    @Test
    @SneakyThrows
    void shouldReturnNotFoundForNotExistingUserUpdate() {
        //GIVEN
        User userEntity = createUserEntity();
        var userId = userRepository.save(userEntity).getId();
        var wrongUserId = UUID.randomUUID();
        UserUpdateDto userInput = createUserUpdateDto(userId);
        String jsonInput = objectMapper.writeValueAsString(userInput);
        //THEN
        mockMvc.perform(put("/users/" + wrongUserId).header("Content-Type", "application/json")
                                         .content(jsonInput))
               .andExpect(status().isNotFound())
               .andExpect(jsonPath("$").value("User with id: " + wrongUserId + " does not exists"));
        assertThat(userRepository.findById(userId)).isPresent()
                                                   .get()
                                                   .hasFieldOrPropertyWithValue("lastName", "Workshop");
    }

    @Test
    @SneakyThrows
    void shouldUpdateUser() {
        //GIVEN
        User userEntity = createUserEntity();
        var userId = userRepository.save(userEntity)
                                   .getId();
        UserUpdateDto userInput = createUserUpdateDto(userId);
        userInput.setLastName("new Last name");
        String jsonInput = objectMapper.writeValueAsString(userInput);
        //THEN
        mockMvc.perform(put("/users/" + userId).header("Content-Type", "application/json")
                                               .content(jsonInput))
               .andExpect(status().isOk());
        assertThat(userRepository.findById(userId)).isPresent()
                                                   .get()
                                                   .hasFieldOrPropertyWithValue("lastName", "new Last name");
        verify(kafkaTemplate).send(any(ProducerRecord.class));
    }

    @Test
    @SneakyThrows
    void shouldNotChangeUserStateToWrongState() {
        //GIVEN
        User userEntity = createUserEntity();
        var userId = userRepository.save(userEntity)
                                   .getId();
        //THEN
        mockMvc.perform(patch("/users/" + userId + "?newState=wrongState").header("Content-Type", "application/json"))
               .andExpect(status().isBadRequest())
               .andExpect(jsonPath("$").value("User state with name: wrongState does not exists"));
        assertThat(userRepository.findById(userId)).isPresent();
        assertThat(userRepository.findById(userId)
                                 .get()
                                 .getState()).isEqualTo(ACTIVE);
    }

    @Test
    @SneakyThrows
    void shouldChangeUserState() {
        //GIVEN
        User userEntity = createUserEntity();
        var userId = userRepository.save(userEntity)
                                   .getId();
        //THEN
        mockMvc.perform(patch("/users/" + userId + "?newState=deleted").header("Content-Type", "application/json"))
               .andExpect(status().isOk());
        assertThat(userRepository.findById(userId)).isPresent();
        assertThat(userRepository.findById(userId)
                                 .get()
                                 .getState()).isEqualTo(DELETED);
        verify(kafkaTemplate).send(any(ProducerRecord.class));
    }

    private UserDto createUserDto(UUID id) {
        return createUserDto(id, now(), "Workshop");
    }

    private UserDto createUserDto(UUID id, LocalDate dateOfBirth, String lastName) {
        return UserDto.builder()
                      .id(id)
                      .firstName("Kafka")
                      .lastName(lastName)
                      .dayOfBirth(dateOfBirth)
                      .address("Aardwark")
                      .state("ACTIVE")
                      .build();
    }


    private UserUpdateDto createUserUpdateDto(UUID id) {
        return UserUpdateDto.builder()
                            .id(id)
                            .firstName("Kafka")
                            .lastName("Workshop")
                            .address("Aardwark")
                            .build();
    }

    private User createUserEntity() {
        return User.builder()
                   .id(UUID.randomUUID())
                   .firstName("Kafka")
                   .lastName("Workshop")
                   .dayOfBirth(now())
                   .address("Aardwark")
                   .state(ACTIVE)
                   .build();
    }


}