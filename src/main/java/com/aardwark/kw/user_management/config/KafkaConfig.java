package com.aardwark.kw.user_management.config;

import com.aardwark.kw.domain_events.user.UserEvent;
import lombok.AllArgsConstructor;
import org.apache.kafka.common.serialization.UUIDSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.Map;
import java.util.UUID;

import static org.apache.kafka.clients.producer.ProducerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.CLIENT_ID_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG;

@Configuration
@AllArgsConstructor
public class KafkaConfig {

    private final UserManagementProperties userManagementProperties;

    @Bean
    public KafkaTemplate<UUID, UserEvent> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }

    @Bean
    public ProducerFactory<UUID, UserEvent> producerFactory() {
        return new DefaultKafkaProducerFactory<>(getProducerConfig());
    }

    private Map<String, Object> getProducerConfig() {
        return Map.of(BOOTSTRAP_SERVERS_CONFIG, userManagementProperties.getBootstrapServer(),
                      CLIENT_ID_CONFIG, userManagementProperties.getProducerId(),
                      KEY_SERIALIZER_CLASS_CONFIG, UUIDSerializer.class,
                      VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
    }
}
