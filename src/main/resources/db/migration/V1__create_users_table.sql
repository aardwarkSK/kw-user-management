CREATE TABLE users (
    id                  UUID       PRIMARY KEY,
    first_name          varchar(100),
    last_name           varchar(200)    NOT NULL,
    day_of_birth        date            NOT NULL,
    address             text,
    state               varchar(50)     NOT NULL

);