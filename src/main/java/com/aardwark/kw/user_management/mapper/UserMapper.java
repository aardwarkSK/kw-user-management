package com.aardwark.kw.user_management.mapper;

import com.aardwark.kw.user_management.controller.dto.UserDto;
import com.aardwark.kw.user_management.controller.dto.UserUpdateDto;
import com.aardwark.kw.user_management.entity.User;

public interface UserMapper {

    /**
     * Map DB entity to client model
     * @param entity DB entity
     * @return mapped application Dto
     */
    UserDto mapToDto(User entity);

    /**
     *  Map client data to DB entity
     * @param user client model
     * @return mapped DB entity
     */
    User mapToEntity(UserDto user);

    /**
     *  Map client data to DB entity
     * @param user client update model
     * @return mapped DB entity
     */
    User mapToEntity(UserUpdateDto user);

}
