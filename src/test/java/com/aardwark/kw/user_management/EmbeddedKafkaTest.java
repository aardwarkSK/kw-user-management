package com.aardwark.kw.user_management;

import com.aardwark.kw.domain_events.user.UserEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.UUIDDeserializer;
import org.junit.jupiter.api.TestInstance;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.test.context.EmbeddedKafka;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import static java.text.MessageFormat.format;
import static java.time.Duration.ofMillis;
import static org.apache.kafka.clients.consumer.ConsumerConfig.AUTO_OFFSET_RESET_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.GROUP_ID_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG;
import static org.apache.kafka.clients.consumer.ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.kafka.support.serializer.JsonDeserializer.TRUSTED_PACKAGES;

@IntegrationTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@EmbeddedKafka(topics = {"user-management"}, partitions = 5,
        ports = 9090, zookeeperPort = 9091,
        brokerProperties = {"broker.id=1"},
        zkConnectionTimeout = 60000, zkSessionTimeout = 60000)
public abstract class EmbeddedKafkaTest {

    public KafkaConsumer<UUID, UserEvent> createUsersConsumer(String bootstrapServer, Set<String> topics,
                                                               String trustedPackages, String groupId) {
        Properties properties = new Properties();
        properties.setProperty(BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
        properties.setProperty(TRUSTED_PACKAGES, trustedPackages);
        properties.setProperty(KEY_DESERIALIZER_CLASS_CONFIG, UUIDDeserializer.class.getName());
        properties.setProperty(VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class.getName());
        properties.setProperty(GROUP_ID_CONFIG, groupId);
        properties.setProperty(AUTO_OFFSET_RESET_CONFIG, "earliest");
        KafkaConsumer<UUID, UserEvent> kafkaConsumer = new KafkaConsumer<>(properties);
        kafkaConsumer.subscribe(topics);
        return kafkaConsumer;
    }

    public <V extends UserEvent> boolean isConsumedMessage(KafkaConsumer<UUID, UserEvent> usersConsumer, V expectedMessage) {
        List<ConsumerRecord<UUID, UserEvent>> messages = new ArrayList<>();
        usersConsumer.poll(ofMillis(500))
                     .forEach(messages::add);
        if(messages.size() != 1) {
            fail(format("Was consumed unexpected number of messages {0}, expected {1}", messages.size(), 1));
        }
        return compareRecords(expectedMessage, messages.get(0));
    }

    private <V extends UserEvent> boolean compareRecords(V expectedMessage, ConsumerRecord<UUID, UserEvent> consumedRecord) {
        if(!expectedMessage.getId().equals(consumedRecord.key())) {
            fail(format("Was consumed unexpected message key {0}, expected {1}", consumedRecord.key(), expectedMessage.getId()));
        }
        if(!expectedMessage.equals(consumedRecord.value())) {
            fail(format("Was consumed unexpected message payload {0}, expected {1}", consumedRecord.value(), expectedMessage));
        }
        return true;
    }
}
