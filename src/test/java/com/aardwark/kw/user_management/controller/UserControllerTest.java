package com.aardwark.kw.user_management.controller;

import com.aardwark.kw.user_management.controller.dto.UserDto;
import com.aardwark.kw.user_management.controller.dto.UserUpdateDto;
import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.mapper.UserMapper;
import com.aardwark.kw.user_management.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static com.aardwark.kw.user_management.entity.UserState.ACTIVE;
import static com.aardwark.kw.user_management.entity.UserState.BLOCKED;
import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@ExtendWith(MockitoExtension.class)
class UserControllerTest {

    @Mock
    private UserService userService;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserController controller;

    private static final UUID USER_ID =  UUID.randomUUID();
    private static final UserDto USER_DTO = createUserDto();
    private static final User USER = createUser();

    @Test
    void doesGetUsers() {
        //GIVEN
        when(userService.getAllUsers()).thenReturn(List.of(USER));
        when(userMapper.mapToDto(any())).thenReturn(USER_DTO);
        //WHEN
        ResponseEntity<?> users = controller.getAllUsers();
        //THEN
        assertThat(users.getStatusCode()).isEqualTo(OK);
        assertThat(users.getBody()).isEqualTo(List.of(USER_DTO));
        verify(userService).getAllUsers();
        verify(userMapper).mapToDto(USER);
    }

    @Test
    void doesHandleNotExistingUser() {
        //GIVEN
        when(userService.getUser(any())).thenReturn(empty());
        //WHEN
        ResponseEntity<?> user = controller.getUser(USER_ID);
        //THEN
        assertThat(user.getStatusCode()).isEqualTo(NOT_FOUND);
        assertThat(user.getBody()).isEqualTo("User with id: " + USER_ID.toString() + " does not exists");
        verify(userService).getUser(USER_ID);
        verifyNoInteractions(userMapper);
    }

    @Test
    void doesGetUser() {
        //GIVEN
        when(userService.getUser(any())).thenReturn(of(USER));
        when(userMapper.mapToDto(any())).thenReturn(USER_DTO);
        //WHEN
        ResponseEntity<?> user = controller.getUser(USER_ID);
        //THEN
        assertThat(user.getStatusCode()).isEqualTo(OK);
        assertThat(user.getBody()).isEqualTo(USER_DTO);
        verify(userService).getUser(USER_ID);
        verify(userMapper).mapToDto(USER);
    }

    @Test
    void doesCreateUser() {
        when(userService.createUser(any())).thenReturn(USER);
        when(userMapper.mapToEntity(any(UserDto.class))).thenReturn(USER);
        when(userMapper.mapToDto(any())).thenReturn(USER_DTO);
        //WHEN
        ResponseEntity<?> newIdResponse = controller.createUser(USER_DTO);
        //THEN
        assertThat(newIdResponse.getStatusCode()).isEqualTo(CREATED);
        assertThat(newIdResponse.getBody()).isEqualTo(USER_DTO);
        verify(userService).createUser(USER);
        verify(userMapper).mapToEntity(USER_DTO);
        verify(userMapper).mapToDto(USER);
    }

    @Test
    void doesUpdateUser() {
        //GIVEN
        when(userMapper.mapToEntity(any(UserUpdateDto.class))).thenReturn(USER);
        //WHEN
        ResponseEntity<?> response = controller.updateUser(USER_ID, new UserUpdateDto());
        //THEN
        assertThat(response.getStatusCode()).isEqualTo(OK);
        verify(userService).updateUser(USER_ID, USER);
    }

    @Test
    void doesNotChangeUserStateForNotExistingState() {
        //WHEN
        ResponseEntity<?> response = controller.changeUserState(USER_ID, "SOMETHING");
        //THEN
        assertThat(response.getStatusCode()).isEqualTo(BAD_REQUEST);
        assertThat(response.getBody()).isEqualTo("User state with name: SOMETHING does not exists");
        verifyNoInteractions(userService);
    }

    @Test
    void doesChangeUserState() {
        //WHEN
        ResponseEntity<?> response = controller.changeUserState(USER_ID, "BLOCKED");
        //THEN
        assertThat(response.getStatusCode()).isEqualTo(OK);
        verify(userService).updateUserState(USER_ID, BLOCKED);
    }

    private static UserDto createUserDto() {
        return UserDto.builder()
                      .id(USER_ID)
                      .firstName("first")
                      .lastName("last")
                      .dayOfBirth(LocalDate.of(2000,1,1))
                      .address("")
                      .state("ACTIVE")
                      .build();
    }

    private static User createUser() {
        return User.builder()
                   .id(USER_ID)
                   .firstName("first")
                   .lastName("last")
                   .dayOfBirth(LocalDate.of(2000,1,1))
                   .address("")
                   .state(ACTIVE)
                   .build();
    }
}