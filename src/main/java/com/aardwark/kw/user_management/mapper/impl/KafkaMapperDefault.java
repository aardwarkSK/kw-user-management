package com.aardwark.kw.user_management.mapper.impl;

import com.aardwark.kw.domain_events.user.UserCreated;
import com.aardwark.kw.domain_events.user.UserStateChanged;
import com.aardwark.kw.domain_events.user.UserUpdated;
import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.entity.UserState;
import com.aardwark.kw.user_management.mapper.KafkaMapper;

import java.util.UUID;

public class KafkaMapperDefault implements KafkaMapper {

    @Override
    public UserCreated mapToCreated(User user) {
        return UserCreated.builder()
                          .id(user.getId())
                          .address(user.getAddress())
                          .dayOfBirth(user.getDayOfBirth())
                          .firstName(user.getFirstName())
                          .lastName(user.getLastName())
                          .state(user.getState().name())
                          .build();

    }

    @Override
    public UserUpdated mapToUpdated(User user) {
        return UserUpdated.builder()
                          .id(user.getId())
                          .firstName(user.getFirstName())
                          .lastName(user.getLastName())
                          .address(user.getAddress())
                          .build();
    }

    @Override
    public UserStateChanged mapToStateChanged(final UUID id, final UserState oldState, final UserState newState) {
        return UserStateChanged.builder()
                              .id(id)
                              .oldState(oldState.name())
                              .newState(newState.name())
                              .build();
    }
}
