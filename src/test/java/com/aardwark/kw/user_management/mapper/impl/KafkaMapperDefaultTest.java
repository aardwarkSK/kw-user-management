package com.aardwark.kw.user_management.mapper.impl;

import com.aardwark.kw.domain_events.user.UserCreated;
import com.aardwark.kw.domain_events.user.UserStateChanged;
import com.aardwark.kw.domain_events.user.UserUpdated;
import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.entity.UserState;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.UUID;

import static com.aardwark.kw.user_management.entity.UserState.ACTIVE;
import static com.aardwark.kw.user_management.entity.UserState.DELETED;
import static org.assertj.core.api.Assertions.assertThat;


class KafkaMapperDefaultTest {

    public static final UUID ID = UUID.randomUUID();
    public static final String FIRST_NAME = "Jozko";
    public static final String LAST_NAME = "Dezko";
    public static final String ADDRESS = "ABCD";
    public static final LocalDate DAY_OF_BIRTH = LocalDate.of(2000, 1, 1);
    public static final UserState USER_STATE = ACTIVE;
    private final KafkaMapperDefault mapper = new KafkaMapperDefault();

    @Test
    void doesMapToCreated() {
        //GIVEN
        User userEntity = createUserEntity();
        //WHEN
        UserCreated mappedEvent = mapper.mapToCreated(userEntity);
        //THEN
        assertThat(mappedEvent).isNotNull();
        assertThat(mappedEvent.getId()).isEqualTo(ID);
        assertThat(mappedEvent.getFirstName()).isEqualTo(FIRST_NAME);
        assertThat(mappedEvent.getLastName()).isEqualTo(LAST_NAME);
        assertThat(mappedEvent.getAddress()).isEqualTo(ADDRESS);
        assertThat(mappedEvent.getDayOfBirth()).isEqualTo(DAY_OF_BIRTH);
        assertThat(mappedEvent.getState()).isEqualTo(USER_STATE.name());
    }

    @Test
    void doesMapToUpdated() {
        //GIVEN
        User userEntity = createUserEntity();
        //WHEN
        UserUpdated mappedEvent = mapper.mapToUpdated(userEntity);
        //THEN
        assertThat(mappedEvent).isNotNull();
        assertThat(mappedEvent.getId()).isEqualTo(ID);
        assertThat(mappedEvent.getFirstName()).isEqualTo(FIRST_NAME);
        assertThat(mappedEvent.getLastName()).isEqualTo(LAST_NAME);
        assertThat(mappedEvent.getAddress()).isEqualTo(ADDRESS);
    }

    @Test
    void doesMapToUserStateChange() {
        //WHEN
        UserStateChanged mappedEvent = mapper.mapToStateChanged(ID, ACTIVE, DELETED);
        //THEN
        assertThat(mappedEvent).isNotNull();
        assertThat(mappedEvent.getId()).isEqualTo(ID);
        assertThat(mappedEvent.getOldState()).isEqualTo(ACTIVE.name());
        assertThat(mappedEvent.getNewState()).isEqualTo(DELETED.name());
    }

    private User createUserEntity() {
        return User.builder()
                   .id(ID)
                   .firstName(FIRST_NAME)
                   .lastName(LAST_NAME)
                   .dayOfBirth(DAY_OF_BIRTH)
                   .address(ADDRESS)
                   .state(USER_STATE)
                   .build();
    }
}