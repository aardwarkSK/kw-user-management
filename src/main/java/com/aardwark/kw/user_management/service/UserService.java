package com.aardwark.kw.user_management.service;

import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.entity.UserState;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {

    /**
     * Get list of all users
     * @return list of users - User.class
     */
    List<User> getAllUsers();

    /**
     * Return user base on id
     * @param id identifier of user in DB
     * @return Optional user data - User.class
     */
    Optional<User> getUser(final UUID id);

    /**
     * create user
     * @param user Object represents new user
     * @return User.class represents new user in DB
     * @throws UserServiceException for unexpected issue
     */
    User createUser(final User user) throws UserServiceException;

    /**
     * Update user record
     * @param id identifier of user in DB
     * @param  user Object represents user
     * @return User.class represents new user in DB
     * @throws UserNotFoundException for not existing user
     */
    User updateUser(final UUID id, final User user) throws UserNotFoundException;

    /**
     * Update user record
     * @param id identifier of user in DB
     * @param  newState new user state
     * @throws UserNotFoundException for not existing user
     */
    void updateUserState(final UUID id, final UserState newState) throws UserNotFoundException;

    /**
     * Delete user
     * @param id - identifier of user in DB
     * @throws UserNotFoundException for not existing user
     */
    void deleteUser(final UUID id) throws UserNotFoundException;
}
