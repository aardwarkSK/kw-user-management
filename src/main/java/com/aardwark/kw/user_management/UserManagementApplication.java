package com.aardwark.kw.user_management;

import com.aardwark.kw.user_management.config.UserManagementConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserManagementConfig.class, args);
    }

}
