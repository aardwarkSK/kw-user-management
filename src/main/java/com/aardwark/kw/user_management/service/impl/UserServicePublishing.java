package com.aardwark.kw.user_management.service.impl;

import com.aardwark.kw.domain_events.user.UserAction;
import com.aardwark.kw.domain_events.user.UserEvent;
import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.entity.UserState;
import com.aardwark.kw.user_management.mapper.KafkaMapper;
import com.aardwark.kw.user_management.repository.UserRepository;
import com.aardwark.kw.user_management.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.aardwark.kw.domain_events.user.UserAction.CREATED;
import static com.aardwark.kw.domain_events.user.UserAction.UPDATED;
import static java.lang.Math.abs;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.kafka.support.KafkaHeaders.CORRELATION_ID;

@Slf4j
public class UserServicePublishing implements UserService {
    private static final int PARTITION_COUNT = 5;

    private final KafkaTemplate<UUID, UserEvent> kafkaTemplate;
    private final KafkaMapper kafkaMapper;
    private final String userTopic;
    private final UserServiceDefault userServiceDefault;

    public UserServicePublishing(UserRepository userRepository,
                                 KafkaTemplate<UUID, UserEvent> kafkaTemplate,
                                 KafkaMapper kafkaMapper,
                                 String userTopic) {
        this.userServiceDefault = new UserServiceDefault(userRepository);
        this.kafkaTemplate = kafkaTemplate;
        this.kafkaMapper = kafkaMapper;
        this.userTopic = userTopic;
    }

    @Override
    public List<User> getAllUsers() {
        return userServiceDefault.getAllUsers();
    }

    @Override
    public Optional<User> getUser(final UUID id) {
        return userServiceDefault.getUser(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public User createUser(final User user) {
        var createdUser = userServiceDefault.createUser(user);
        ProducerRecord<UUID, UserEvent> kafkaMessage = createKafkaMessage(createdUser.getId(),
                                                                          kafkaMapper.mapToCreated(createdUser),
                                                                          CREATED);
        kafkaTemplate.send(kafkaMessage);
        return createdUser;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public User updateUser(final UUID id, final User user) {
        var updatedUser = userServiceDefault.updateUser(id, user);
        ProducerRecord<UUID, UserEvent> kafkaMessage = createKafkaMessage(user.getId(),
                                                                          kafkaMapper.mapToUpdated(updatedUser),
                                                                          UPDATED);
        kafkaTemplate.send(kafkaMessage);
        return updatedUser;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateUserState(final UUID id, final UserState newState) {
        UserState oldState = userServiceDefault.getUser(id)
                                                .map(User::getState)
                                                .orElse(null);
        userServiceDefault.updateUserState(id, newState);
        ProducerRecord<UUID, UserEvent> kafkaMessage = createKafkaMessage(id,
                                                                          kafkaMapper.mapToStateChanged(id, oldState, newState),
                                                                          UPDATED);
        kafkaTemplate.send(kafkaMessage);
    }

    @Override
    public void deleteUser(final UUID id) {
        userServiceDefault.deleteUser(id);
    }

    private ProducerRecord<UUID, UserEvent> createKafkaMessage(final UUID userId, final UserEvent event, final UserAction action) {
        int partition = calculatePartition(userId);
        List<Header> headers = createHeaders(userId, action);
        return new ProducerRecord<>(userTopic, partition, userId, event, headers);
    }

    private int calculatePartition(final UUID userId) {
        return abs(userId.hashCode()) % PARTITION_COUNT;
    }

    private List<Header> createHeaders(final UUID userId, final UserAction action) {
        return List.of(new RecordHeader(CORRELATION_ID, uuidAsBytes(userId)),
                       new RecordHeader("ACTION", action.name().getBytes(UTF_8)));
    }

    private byte[] uuidAsBytes(UUID uuid) {
        var buffer = ByteBuffer.wrap(new byte[32]);
        buffer.putLong(uuid.getMostSignificantBits());
        buffer.putLong(uuid.getLeastSignificantBits());
        return buffer.array();
    }
}
