package com.aardwark.kw.user_management.mapper.impl;

import com.aardwark.kw.user_management.controller.dto.UserDto;
import com.aardwark.kw.user_management.controller.dto.UserUpdateDto;
import com.aardwark.kw.user_management.entity.User;
import com.aardwark.kw.user_management.mapper.UserMapper;

import static com.aardwark.kw.user_management.entity.UserState.valueOf;

public class UserMapperDefault implements UserMapper {

    @Override
    public UserDto mapToDto(User entity) {
        return UserDto.builder()
                      .id(entity.getId())
                      .firstName(entity.getFirstName())
                      .lastName(entity.getLastName())
                      .dayOfBirth(entity.getDayOfBirth())
                      .address(entity.getAddress())
                      .state(entity.getState()
                                   .name())
                      .build();
    }

    @Override
    public User mapToEntity(UserDto user) {
        return User.builder()
                   .firstName(user.getFirstName())
                   .lastName(user.getLastName())
                   .dayOfBirth(user.getDayOfBirth())
                   .address(user.getAddress())
                   .state(valueOf(user.getState()))
                   .build();
    }

    @Override
    public User mapToEntity(UserUpdateDto user) {
        return User.builder()
                   .id(user.getId())
                   .firstName(user.getFirstName())
                   .lastName(user.getLastName())
                   .address(user.getAddress())
                   .build();
    }
}
