package com.aardwark.kw.user_management.config;

import com.aardwark.kw.domain_events.user.UserEvent;
import com.aardwark.kw.user_management.controller.error_handler.RestExceptionHandler;
import com.aardwark.kw.user_management.mapper.KafkaMapper;
import com.aardwark.kw.user_management.mapper.UserMapper;
import com.aardwark.kw.user_management.mapper.impl.KafkaMapperDefault;
import com.aardwark.kw.user_management.mapper.impl.UserMapperDefault;
import com.aardwark.kw.user_management.repository.UserRepository;
import com.aardwark.kw.user_management.service.UserService;
import com.aardwark.kw.user_management.service.impl.UserServicePublishing;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.UUID;

@Configuration
@ComponentScan(basePackages = {"com.aardwark.kw.user_management"})
@EnableJpaRepositories(basePackages = "com.aardwark.kw.user_management.repository")
@EntityScan(basePackages = "com.aardwark.kw.user_management.entity")
@AllArgsConstructor
public class UserManagementConfig {

    private final UserManagementProperties userManagementProperties;

    @Bean
    public UserMapper userMapper() {
        return new UserMapperDefault();
    }

    @Bean
    public KafkaMapper kafkaMapper() {
        return new KafkaMapperDefault();
    }

    @Bean
    public UserService userService(final UserRepository userRepository,
                                   final KafkaTemplate<UUID, UserEvent> kafkaTemplate,
                                   final KafkaMapper kafkaMapper,
                                   @Value("${spring.kafka.user.management.topic}") String userTopic) {
        return new UserServicePublishing(userRepository, kafkaTemplate, kafkaMapper, userTopic);
    }

    @Bean(name = "restExceptionHandlerBean")
    public RestExceptionHandler restExceptionHandlerBean() {
        return new RestExceptionHandler();
    }

}
