FROM adoptopenjdk/openjdk11:jre-11.0.6_10-alpine

EXPOSE 8080

ARG JAR_FILE
ADD  target/${JAR_FILE} users-service.jar

ENTRYPOINT ["java", "-jar", "users-service.jar"]
